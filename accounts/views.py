import json
import re
from django.core.files.images import get_image_dimensions
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, reverse
from django.utils.decorators import method_decorator
from django.views import View
from .models import CustomUser
# Create your views here.

class Home(View):
    def get(self,request):
        return render(request,'home.html')


def validate_mobile_num(value):
    pattern = re.compile("^[6-9]\d{9}$")
    if not pattern.search(value):
        return False
    return True

def validate_password(password):
    pattern = re.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})")
    if not pattern.search(password):
        return False
    return True

class SignUpView(View):

    def __init__(self, *args, **kwargs):
        self.data = {}
        self.context = {}

    def get(self,request):
        return render(request, 'signup.html')

    def post(self, request):
        self.data = request.POST
        self.context["data"] = self.data
        name = self.data.get('name', None)
        password = self.data.get('password', None)
        if request.FILES:
            image = request.FILES['image']
        else:
            image = None
        dob = self.data.get('dob', None)
        phone = self.data.get('phone', None)
        user_type = self.data.get('user_type', None)
        if not name or not password or not image or not dob or not phone or not user_type:
            self.context["error_message"] = "Please Fill All Fields"
            self.context["name"] = name
            self.context["password"] = password
            self.context["dob"] = dob
            self.context["phone"] = phone
            self.context["user_type"] = user_type
            # self.context["country"] = country
            return render(request, 'signup.html', self.context)
        ret = validate_mobile_num(phone)
        if not ret:
            self.context["error_message"] = "Mobile Number not valid"
            self.context["name"] = name
            self.context["password"] = password
            self.context["dob"] = dob
            self.context["phone"] = phone
            self.context["user_type"] = user_type
            return render(request, 'signup.html', self.context)

        if image:
            w, h = get_image_dimensions(image)
            print(w,h)
            if w != 271 or h != 398:
                self.context["error_message"] = "image dimensions should be 280*180"
                self.context["name"] = name
                self.context["password"] = password
                self.context["dob"] = dob
                self.context["phone"] = phone
                self.context["user_type"] = user_type
                return render(request, 'signup.html', self.context)

        ret = validate_password(password)
        if not ret:
            self.context["error_message"] = "Password must contain 1 uppercase 1 lowercase symbols and numbers"
            self.context["name"] = name
            self.context["password"] = password
            self.context["dob"] = dob
            self.context["phone"] = phone
            self.context["user_type"] = user_type
            return render(request, 'signup.html', self.context)

        user_obj = CustomUser()
        user_obj.name = user_obj.username = name
        user_obj.set_password(password)
        if user_type == "admin":
            user_obj.is_superuser = True
        user_obj.phone = phone
        user_obj.dob = dob
        user_obj.avatar = image
        user_obj.save()
        login(request, user_obj)
        if user_obj.is_superuser:
           return redirect("/login")
        return redirect('/login')


class LoginView(View):

    def __init__(self, *args, **kwargs):
        self.context = {}

    def get(self, request):
        return render(request, "login.html")

    def post(self, request):
        username = request.POST.get("name", None)
        password = request.POST.get("password", None)

        if not username or not password:
            self.context["error_message"] = "Please fill both fields"
            self.context["name"] = username
            self.context["password"] = password
            return render(request, "login.html", self.context)
        try:
            user = CustomUser.objects.get(username=username)
        except:
            self.context["error_message"] = "User doesn't exist"
            self.context["name"] = username
            self.context["password"] = password
            return render(request, "login.html", self.context)
        user_obj = authenticate(username=username, password=password)
        if user_obj:
            login(request,user_obj)
        else:
            self.context["error_message"] = "Invalid Credentials"
            self.context["name"] = username
            self.context["password"] = password
            return render(request, "login.html", self.context)
        return redirect("/")


class DashboardView(View):

    @method_decorator(login_required)
    def get(self,request):
        context = {}
        if request.user.is_superuser:
            context["students"] = CustomUser.objects.all()
        else:
            context["student"] = request.user
        return render(request, "dashboard.html", context)

    def post(self, request):
        print(self.request.POST)
        id = request.POST.get("id", None)
        mark = request.POST.get("mark", None)
        if not mark:
            data = {}
            data['result'] = "Please enter the mark"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        user = CustomUser.objects.get(id=int(id))
        user.marks = mark
        user.save()
        data = {}
        data['result'] = "success"
        return HttpResponse(json.dumps(data),
                            content_type="application/json")



class LogoutView(View):

    def get(self, request):
        logout(request)
        return redirect('login')

class UserProfileView(View):

    def __init__(self, *args, **kwargs):
        self.context = {}

    @method_decorator(login_required)
    def get(self, request):
        id = request.GET.get("id", None)
        user = CustomUser.objects.get(id=int(id))
        self.context["user"] = user
        return render(request, "profile.html", self.context)

    def post(self, request):
        print(self.request.POST)
        print(request.FILES["image"])
        id = request.POST.get("id", None)
        print(id)
        name = request.POST.get("name", None)
        phone = request.POST.get("phone", None)
        dob = request.POST.get("dob", None)
        print(dob)
        if request.FILES:
            image = request.FILES['image']
        else:
            image = None
        if not name or not phone or not dob or not image:
            data = {}
            data['result'] = "Please fill all fields"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        ret = validate_mobile_num(phone)
        if not ret:
            data = {}
            data['result'] = "invalid mobile number"
            return HttpResponse(json.dumps(data),
                                content_type="application/json")
        if image:
            w, h = get_image_dimensions(image)
            print(w,h)
            if w != 271 or h != 398:
                data = {}
                data['result'] = "image dimensions must be 180*280"
                return HttpResponse(json.dumps(data),
                                    content_type="application/json")
        user = CustomUser.objects.get(id=int(id))
        user.name = name
        user.username = name
        user.phone = phone
        user.dob = dob
        user.avatar = image
        user.save()
        data = {}
        data['result'] = "success"
        data['id'] = id
        return HttpResponse(json.dumps(data),
                            content_type="application/json")