from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from datetime import date


# Create your models here.

class CustomUser(AbstractUser):
    name = models.CharField(_('Name'), null=True, blank=True, max_length=50)
    avatar = models.FileField(_('Image'), upload_to='user_images', null=True, blank=True)
    dob = models.DateField(_('Date of Birth'), null=True, blank=True)
    phone = models.CharField(_('Phone number'), null=True, blank=True, max_length=10)
    marks = models.IntegerField(_('Marks'),null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_age(self):
        today = date.today()
        return today.year - self.dob.year - ((today.month, today.day) < (self.dob.month, self.dob.day))


