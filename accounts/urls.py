from django.urls import path
from .views import SignUpView, LoginView, DashboardView, LogoutView, UserProfileView,Home
urlpatterns = [
    path('', DashboardView.as_view(), name="dashboard"),
    path('home/',Home.as_view()),
    path('signup/', SignUpView.as_view(), name="signup"),
    path('login/', LoginView.as_view(), name="login"),
    path('logout/', LogoutView.as_view(), name="logout"),
    path('user-profile/', UserProfileView.as_view(), name="user-profile")

]